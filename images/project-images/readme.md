 # Convert Large Images to WEBP and Scale to max 800x800px

 ```bash
 mogrify -resize 800x800 -format webp -quality 80 *.jpg
 ```