// Header Logo Scroll Opacity
const bannerLogo = document.querySelector('#banner-logo');
const navLogo = document.querySelector('#navigation-logo');
const logo = document.querySelector('#logo');
const socialIcons = document.querySelector('.social-icons');
const menu = document.querySelector('.menu');
const menuCheck = document.querySelector('.menu-btn');

// Add a scroll event listener to the window
window.addEventListener('scroll', function() {
  // Get the current scroll position
  const scrollTop = window.scrollY;

  // Calculate the opacity of the element based on the scroll position
  const bannerOpacity = 1 - scrollTop / 100;
  const navOpacity = 0 + scrollTop / 100;
  const socialOpacity = 1 - scrollTop / 300;

  // Set the opacity of the element
  bannerLogo.style.opacity = bannerOpacity;
  navLogo.style.opacity = navOpacity;
  socialIcons.style.opacity = socialOpacity;

  // Remove social icons entirely if scroll is past 300px.
  if (scrollTop > 300) {
    socialIcons.style.display = 'none';
  } else {
    socialIcons.style.display = 'block';
  }

});

menu.addEventListener('click', function() {
    menuCheck.checked = false;
});

logo.addEventListener('click', function() {
    menuCheck.checked = false;
});

document.getElementById("year").innerHTML = new Date().getFullYear();

// Contact form AJAX
if (document.getElementById('contact')) {
  document.addEventListener('DOMContentLoaded', function() {
    document.querySelector('#contact-form').addEventListener('submit', function (event) {
      var thank_you = document.querySelector("#thank-you");
      const form = new FormData(document.getElementById("contact-form"));
      const hp = document.getElementById("work-phone");
      const email = document.getElementById("email");
      event.preventDefault();
      if (hp.value == '' && email.value != '') {
        fetch("https://docs.google.com/forms/u/3/d/e/1FAIpQLSdEMafzNqFwwScLUUQcPDgBs-ieRBzr-pohp3pqbl4Tjzk_QQ/formResponse", {
          method: "POST",
          body: form,
        })
        .then(response => response.json())
        .catch(error => console.log(error))
        .then(response => console.log(response))
        .catch(error => console.log(error));
        this.style.display = 'none';
        thank_you.style.display = 'block';
      }
    });
  });
}